### Цикл for (з параметром)
```cpp
int s=0; 
for(int i=1; i<10; i++){
     s=s+i; 
    }
```

### Цикл while (з передумовою)
```cpp
int i=1,s=0; 
while(i<10){ 
    s=s+i; 
    i++; 
    }
```

### Цикл do..while (з післяумовою)
```cpp
int s=0, i=1; 
do{ 
    s=s+i; 
    i++; 
} while(i<10);

```


