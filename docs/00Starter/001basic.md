### Структура програми 
```cpp
#include <iostream> 
using namespace std; 
int main() { 
    ... 
    return 0; 
}
```

### Введення даних 
```cpp
cin>>a; 
```

### Виведення даних 
```cpp
cout<<"S="<<S<<endl; 
```

### Форматований вивід 
```cpp
#include <iomanip> 
...
cout<<fixed<<setprecision(2)<<S;
```

### Типи даних 
```cpp
int (signed, unsigned, short, long) 
float (double, long double) 
char 
string
bool
```

### Оголошення змінних 
```cpp
int a,b; 
unsigned int c=10; 
short d{200};   //ініціалізація з перевіркою
char letter='A'; 
string word="Hello"; 
bool flag=true; 
```


### Математичні операції та функції
```cpp
+ - * / % (ост.) 

#include <math.h> 
x=sqrt(a); 
```

### Логічні оператори 
```cpp
< > <= >= != == 
```

Службові слова складних умов 
```cpp
&& (and) 
|| (or) 
! (not)
```



