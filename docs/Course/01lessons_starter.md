## :fontawesome-solid-star:{ .star } Стартовий

### :fontawesome-brands-youtube:{ .youtube } Домашнє завдання 
Створення простого консольного калькулятору засобами мови С++. 

[Дивитись :fontawesome-brands-youtube:](https://youtu.be/7_ILC3f_opk?feature=shared){ .md-button .md-button--primary}

---


### :fontawesome-brands-youtube:{ .youtube } Приготування

Реєстрація на ресурсі [https://www.eolymp.com](https://www.eolymp.com/uk/).  Приклад написання та тестування програми в автоматизованому середовищі.

[Дивитись :fontawesome-brands-youtube:](https://youtu.be/6X_rKmlwPKs?feature=shared){ .md-button .md-button--primary}

---


### :fontawesome-brands-youtube:{ .youtube } Урок 1. Починаємо програмувати

Переваги мови програмування С++. Історія створення. Поняття компілятора та середовища програмування. Перша програма. Типи даних. Математичні функції.

[Дивитись :fontawesome-brands-youtube:](https://youtu.be/TyRE4peOmvs?feature=shared){ .md-button .md-button--primary}

---


### :fontawesome-brands-youtube:{ .youtube } Урок 2. Команда розгалуження

Типи мов програмування. Транслятори. Призначення операторів. Вказівка розгалуження.

[Дивитись :fontawesome-brands-youtube:](https://youtu.be/-qUN_NeO-sg?feature=shared){ .md-button .md-button--primary}

---


### :fontawesome-brands-youtube:{ .youtube } Урок 3. Складена умова. Розв'язування задач

Повторення. Розв'язування задач на розгалуження. Приклади складених умов. Скорочена форма розгалуження. 

[Дивитись :fontawesome-brands-youtube:](https://youtu.be/EddDST-Zotg?feature=shared){ .md-button .md-button--primary}

---


### :fontawesome-brands-youtube:{ .youtube } Урок 4. Класичні задачі з розгалуженням. Множинний вибір

Розбір класичних задач з розгалуженням (парне/непарне, числа одного знаку, одноцифрове число). Оператор switch-case.

[Дивитись :fontawesome-brands-youtube:](https://youtu.be/moX6U3RSQSo?feature=shared){ .md-button .md-button--primary}

---


### :fontawesome-brands-youtube:{ .youtube } Урок 5. Використання оператору множинного вибору 

Повторення. Використання оператору switch-case. Розв'язування задач.

[Дивитись :fontawesome-brands-youtube:](https://youtu.be/RyshBckyi94?feature=shared){ .md-button .md-button--primary}

---


### :fontawesome-brands-youtube:{ .youtube } Урок 6. Типи даних. Робота з файлами

Повторення. Огляд базових типів даних на мові С++. Поняття потоків введення/виведення. Робота з файлами.

[Дивитись :fontawesome-brands-youtube:](https://youtu.be/d3lUQR-uNFA?feature=shared){ .md-button .md-button--primary}


---

### :fontawesome-brands-youtube:{ .youtube } Урок 7-8. Цикли (І).

Цикли в мові С++. Опис та практичне використання. Класичні задачі на використання команд повторення.

[Дивитись :fontawesome-brands-youtube:](https://youtu.be/ew-Jdlwi8Wc?feature=shared){ .md-button .md-button--primary}


---


### :fontawesome-brands-youtube:{ .youtube }  Урок 9. Цикли (ІІ)

Концепція структурного програмування. Використання циклу з параметром (for). Табулювання функцій. 

[Дивитись :fontawesome-brands-youtube:](https://youtu.be/uhophDkFeRA?feature=shared){ .md-button .md-button--primary}


---


### :fontawesome-brands-youtube:{ .youtube } Урок 10. Цикли (ІІІ)

Цикл з передумовою (while). Задача на обчислення суми цифр числа. Використання операцій цілочисельного ділення та остачі від ділення.

[Дивитись :fontawesome-brands-youtube:](https://youtu.be/VFvoiYzjVoM?feature=shared){ .md-button .md-button--primary}


