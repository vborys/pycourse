## :fontawesome-solid-star:{ .star } :fontawesome-solid-star:{ .star } Базовий

### :fontawesome-brands-youtube:{ .youtube } Домашнє завдання 
Проект SuperCalc++

[Дивитись :fontawesome-brands-youtube:](https://youtu.be/-ad6aCewVHk?feature=shared){ .md-button .md-button--primary}

---


### :fontawesome-brands-youtube:{ .youtube } Урок 12*. Системне ПЗ
СПЗ. Огляд операційних систем. ОС GNU/Linux. Командний інтерпретатор bash. Віртуальні машини.


[Дивитись :fontawesome-brands-youtube:](https://youtu.be/QjGRnSpACdI?feature=shared){ .md-button .md-button--primary}



---



### :fontawesome-brands-youtube:{ .youtube } Урок 13-14*. Файлові системи
Файлові системи. Способи зберігання файлів. Задача на реверс числа.


[Дивитись :fontawesome-brands-youtube:](https://youtu.be/WnXbhhuK97A?feature=shared){ .md-button .md-button--primary}



---



### :fontawesome-brands-youtube:{ .youtube } Урок 15. Функції
Функції. Користувацькі функції. Опис (сигнатура) функції. Перевантаження функцій.


[Дивитись :fontawesome-brands-youtube:](https://youtu.be/-JEoOU0qZoc?feature=shared){ .md-button .md-button--primary}



---



