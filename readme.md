# Опорний конспект Python3 для початківців(v2.1)

## ЗМІННІ
```py
myNumber = 3   #myNumber->3
name = "John" 
```

## ТИПИ ДАНИХ
```py
str, int, float, bool, complex, NoneType
```

### Перетворення типів
```py
int() float() bin() str() bool() list() dict() set() tuple()
```

### Перевірка типу
```py
print(type(a))		#<class 'int'>
isinstance(a, int)	#True/False
```

## ВВЕДЕННЯ ДАНИХ
```py
word = input()
num = int(input())
word1,word2 = input().split()
num1,num2 = map(int,input().split())
```


## ВИВЕДЕННЯ ДАНИХ
```py
print(b)
print(a1,a2)
print(a,b,sep = "-")  #виведення із роздільником
print(a,b,end = " ")  #виведення із закінченням
print("Hello"*3)      #реплікація (повторне виведення)
```

### Форматований вивід 
```py
#f-string
first_name = "ada"
last_name = "lovelace"
print(f"{first_name} {last_name}")

#r-string
print(r"c:\user\tasks\new")

#.format()
first_name = "ada"
last_name = "lovelace"
print("The first programmer was {0} {1}".format(first_name,last_name))

#float format output
price = 196.3587
print(“{:.2f}”.format(price))
print(f"{price:.2f}")
```


## МАТЕМАТИЧНІ ОПЕРАЦІЇ
```py
+  -   *   /  // %  **
```

### Використання матем.функцій
```py
import math
x = math.sqrt(a)
```

### Логічні оператори
```py
<	>	<= 	>= 	!= 	 == 
```

### Службові слова складних умов
```py
and or not in
```

### Випадкові числа
```py
from random import *
n = randint(1,10)
```

## КОМЕНТАРІ
```py
# однорядковий коментар

'''
багаторядковий 
коментар
'''
```

## РОЗГАЛУЖЕННЯ if
```py
if x > 0 and x < 5:
   print(x)
elif x>= 5:
   print(x*x)
else:
   print(“error”)
```

### Тернарний оператор
```py
max = a if a > b else b 
```


## ЦИКЛИ 

### Цикл з параметром for
```py
s = 0
for i in range(1,10,1):
   s = s+i
print(s)
```

### Цикл з передумовою while
```py
s = 0
i = 1
while i <= 10:
   s = s+i
   i = i+1
print(s)
```

### Додаткові команди для циклів
```py
continue	#перезапуск ітерації
break		#вихід з циклу
```



## РЯДКИ
```py
st = input()

#використання операторів
print(‘Hello’ + ‘ world’)
print(‘Hello ’* 5)
print(‘w’ in st)
```

### Методи рядків
```py
name  =  "ada lovelace"
print(name.upper())		#ADA LOVELACE
print(name.lower())		#ada lovelace
print(name.title())		#Ada Lovelace
.isnumeric
.isalpha

wd = s.split()   //розділення
‘-’ .join(sl)  //злиття з роздільником
s.find(‘red’) //пошук входження (-1, немає)
s.replace(‘yes’,’no’)
```

### Зрізи
```py
print(st[3:9])
print(st[3:9:2])
```





## СПИСОК (list)
```py
#введення та ініціалізація списку
textList = list(input().split())
numList = list(map(int,input().split()))

mas = [1, 5, 7, 9]
mas = ["John", "Bob", "Dan"]

numbers = list(range(1,10))

#генераторні списки
squares = [element**2 for element in range(1,11)]

#доступ до елементу
print(mas[0])  
```

### Перебір масиву:
```py
#поелементно
for element in mas:
	print(element)
#за індексами
for i in range (0,n):
	print(mas[i])
```

### Функції роботи зі списками
```py
sumList = sum(mas)         
minElement = min(mas)
maxElement = max(mas)        
lengthList = len(mas)
```

### Методи списків
```py
mas.sort()				#mas.sort(reverse = True)
mas.remove(element)
mas.reverse()
mas.clean()
mas.append(element)
mas.index(element)
mas.insert(position,element)
mas.pop(position)
mas.count(element)
```

### Зрізи списку (слайси)
```py
print(mas[2:4])
print(mas[:3])
copymas = mas[:]
```


## КОРТЕЖ (tuple) 
```py
#незмінний список
a = (10, 2.13, “square”, 89, ‘C’)
b = tuple(b)
(a1,a2,a3,a4,a5)  =  a 	#розпаковка
```


## ФУНКЦІЇ

### Описи і виклики функцій з відомою кількістю аргументів
```py
RESULT = 0				#глобальна змінна
def sum_num(a,b=0):		#функція зі звичайним і параметром по-замовчуванню
	return a+b

x,y = int(input())
RESULT = sum_num(x,y)			#виклик функції з позиційними аргументами
RESULT = sum_num(a = x, b = y)	#виклик функції з іменованими аргументами
print(RESULT)
```

### Описи і виклики функцій з невідомою кількістю аргументів
```py
def calc(operation, *args):		#упаковка в кортеж n позиційних аргументів
	result = 0
    for arguments in args:
		if operation == "+":
			result += arguments
    return result

print (calc("+", 1,2,3,4))
```

```py
def in_dict(**kwargs):			#упаковка в словник n іменованих аргументів 
    print(kwargs)

in_dict(cat = ’кіт’, dog = ’собака’)
```

### Анотації до функцій (опис очік.типів)
```py
def some_func(a:int, b:str) -> float:
     print(a,b)
     return (3.5)
```

### Анонімні функції (lambda)
```py
g = lambda x,y,z:x+(y*z)
print(g(1,2,3))
```

### Вбудовані функції
-для роботи з символами – ord(), chr(), len()
-математичні – abs(), round(), divmod(), pow(), max(), min(), sum()
 

## МОДУЛІ ТА ПАКЕТИ

Модуль - це файл будь-який файл з розширенням .py
Пакет - це каталог (папка) в якій знаходиться модуль __init__.py

### Імпорти
```py
import math  (random, sys)
from math import *				//імпорт усього модулю	
from math import sqrt			//вибір ф-цій
from math import sqrt as sq		//імпорт з псевдонімом
dir(math)						//перелік функцій у модулі
```





## СЛОВНИК (dict) 
```py
vocabulary = {	
		'cat': 'кіт', 
		'dog': 'собака', 
		'bird': 'птах'
	}

print(vocabulary['cat'])					#друк за ключем
vocabulary['elephant']  =  'бегемот'		#додавання
print(vocabulary)							#друк словника
del vocabulary['cat']						#видалення за ключем
searched_word = vocabulary.get('lion','No searched word')
```

### Методи словників
```py
a.keys()
a.items()
a.values()
a.clear()
```

### Опрацювання словників
```py
for key,value in vocabulary.items():	#перебір циклом
	print(f"\n Key:{key}")
	print(f" Value:{value}")

if 'dog' not in vocabulary.keys():		#пошук за ключем
	print('No key in dict')

```







## РОБОТА З ФАЙЛАМИ 

### ВВЕДЕННЯ
```py
file_object = open("input.txt","r")
a = file_object.readline()
a = int(a)
file_object.close()
```

Читання файлу з автоматичним закриттям
```py
with open("input.txt") as file_object:
  content = file_object.read()           #або lines = file_object.readlines()

print(content.rstrip())
```



### ВИВЕДЕННЯ
```py
file_object = open("output.txt","w")           # r - read, w - write, a - append,  r+ - read/write
b = str(b)
file_object.write(b)
file_object.close()
```

```py
with open("output.txt", "w") as file_object:
  file_object.write("Hello, world! \n")
```





## ВИЙНЯТКИ (exeptions)


### ZeroDivisionError

```py
a = int(input("a="))
b = int(input("b="))
try:
  result = a / b
except ZeroDivisionError:
  print("You can't divide by 0!")
else:
  print(result)
```

### ValueError (NameError, TypeError)

```py
#Перевірка на число
count = input()
try:
   count = int(count)
except ValueError: 
   print("Not digit")
else:
  print(type(count))
```

### FileNotFoundError

```py
filename = "input.txt"
try:
  with open(filename, encoding="utf-8") as f:
    content = f.read()
except FileNotFoundError:
  print(f"File {filename} does not exist!")
else:
  ...
```



## ЗБЕРІГАННЯ ДАНИХ (JSON)

```py
import json
numbers = [2, 3, 5, 7, 11, 13]
filename = "numbers.json"

with open(filename, "w") as f:
  json.dump(numbers, f)         #вивантаження даних

with open(filename, "r") as f:
  numbers = json.load(f)        #завантаження даних

print(numbers)


```










## МНОЖИНА (set)

Множина - це незмінна, невпорядкована колекція унікальних елементів, яка може складатися з цілих чисел, чисел з плаваючою комою, рядків і кортежів. Однак, множини не можуть містити змінні елементи, такі як списки, множини або словники.

```py
set1 = {"John", "Bob", 22, 26}

lst = ["Jenny", 26, "Parker", "Parker", 10.5]
set2 = set(lst)				#перетворення списку в множину

students = {"Jane", "Carlos", "Amy", "Bridgette", "Chau"}
print("Chau" in students)	#перевірка наявності елементу в множині True/False

#МЕТОДИ РОБОТИ З МНОЖИНОЮ
students1 = {"Jane", "Carlos", "Amy", "Bridgette", "Chau"}
students2 = {"Alice", "Lily", "Zhuo", "Amy", "Jane"}
 
students1.update(students2)		#додавання до множини іншої множини, кортежу, списку чи словника

set1.add(<element>)
set1.remove(<element>)
set1.issubset(y)				#True/False
set1.union(y)					#(x|y)об’єднання
set1.intersection(y)			#(x&y)переріз
set1.difference(y)				#(x-y)різниця
set1.symmetric_difference(y)	#(x^y)симетрична різниця
```


## Об'єктно-орієнтоване програмування (ООП)

### Поняття класу

![image](img/oop1.png)


Абстракція (Abstraction) - виділення найістотніших характеристик об'єкта, що відрізняє його від інших

```py
class Person:						#назва класу (PascalCase)
	def __init__(self, name, age):	#конструктор класу
		#ініціалізація атрибутів
		self.name = name
		self.age = age
	
	def get_name(self):				#методи клас (self - вказівник для екземплярів)
		return self.name
	
	def get_age(self):
		return self.age

	def __del__(self):				#деструктор
		pass

person1 = Person (name = "John", age = 34)		#створення екземпляру
print(person1.name, person1.age)
print(person1.get_name(), person1.get_age())	#виклик методів
del person1

```

Поліморфізм (Polymorphism) - коли об'єкти різних класів з різною реалізацією, мають однакові інтерфейси

```py
class Checking():
   def type(self):
       print('You have a checking account at the Codecademy Bank.')
 
   def balance(self):
       print('$20 left in your checking.')
 
class Savings():
   def type(self):
       print('You have a savings account at the Codecademy Bank.')
 
   def balance(self):
       print('$1000 left in your savings.')


account_a = Checking()
account_b = Savings()
 
for account in (account_a, account_b):
   account.type()
   account.balance()

```

Інкапсуляція (Encapsulation) - приховування даних атрибутів від впливів напряму 
 Модифікатори:
 = публічні - доступні ззовні (name)
 = захищені - не наслідуються (_phoneNumber)
 = приватні - недоступні напряму(__creditCard)
 Модифікатори працюють і для методів


Успадкування (Inheritance) - передача властивостей і методів батьківських класів з можливістю зміни

```py
class Person:
    def __init__(self, firstName, lastName, age):
        self.firstName = firstName
        self.lastName = lastName
        self.age = age
        
    def get_Person_info(self):
        return f"Name: {self.firstName} {self.lastName}, Age: {self.age}"

class Student(Person):
    def __init__(self, firstName, lastName, age, phoneNumber, creditCard):
        #Person.__init__(self, firstName, lastName, age)
        super().__init__(firstName, lastName, age)
        self._phoneNumber = phoneNumber
        self.__creditCard = creditCard

    def get_Student_info(self):
        return f"Name: {self.firstName} {self.lastName}, Age: {self.age}, {self._phoneNumber}, {self.__creditCard} "

student1 = Student("John", "Doe", 18, "+380671234567", "4561 1236 7894 1230")
print(student1.get_Student_info())
print(student1._phoneNumber) # protected variable can be accessed
print(student1.__creditCard) # private variable cannot be accessed
```


### "Магічні" методи

Метод __str__() в мові Python використовується для представлення об'єкта у вигляді рядка. Цей метод є одним із методів, який називається "магічними методами" (або "dunder" методами), оскільки він має спеціальне ім'я, яке починається і закінчується на два символи "_".

Метод __str__() визначає рядок, який буде повернутий, коли об'єкт буде виведений за допомогою функції print() або коли використовується функція str(). Цей метод зазвичай використовується для того, щоб забезпечити зрозуміле представлення об'єкта для користувача або для збереження даних у текстовому форматі.

```py
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def __str__(self):
        return "({0}, {1})".format(self.x, self.y)
```

```bash
p = Point(3, 4)
print(p)  # виведе (3, 4)
```












## Черга і стек
Розглянемо два типи структур даних: черги та стеки. Це структури даних, які можуть зберігати колекцію елементів. Вони не є вбудованими у Python, але можна використовувати вбудовані структури даних Python, такі як списки, для їх створення. Різниця між ними полягає в тому, що черга зберігає елементи у форматі "першим прийшов - першим пішов" (FIFO), тоді як стек зберігає елементи у форматі "останнім прийшов - першим пішов" (LIFO).

### Черга

```py
class Queue:
 def __init__(self):
   self.head = None
   self.tail = None
   self.size = 0
 
 def enqueue(self, value):
   if self.has_space():
     item_to_add = Node(value)
     print("Adding " + str(item_to_add.get_value()) + " to the queue!")
     if self.is_empty():
       self.head = item_to_add
       self.tail = item_to_add
     else:
       self.tail.set_next_node(item_to_add)
       self.tail = item_to_add
     self.size += 1
   else:
     print("Sorry, no more room!")
 
 def dequeue(self):
   if self.get_size() > 0:
     item_to_remove = self.head
     print(str(item_to_remove.get_value()) + " is served!")
     if self.get_size() == 1:
       self.head = None
       self.tail = None
     else:
       self.head = self.head.get_next_node()
     self.size -= 1
     return item_to_remove.get_value()
   else:
     print("The queue is totally empty!")
  def peek(self):
   if self.size > 0:
     return self.head.get_value()
   else:
     print("No orders waiting!")
  def get_size(self):
   return self.size
 def is_empty(self):
   return self.size == 0
```

### Стек

```py
from node import Node
 
class Stack:
  def __init__(self, limit=1000):
    self.top_item = None
    self.size = 0
 
  def push(self, value):
    if self.has_space():
      item = Node(value)
      item.set_next_node(self.top_item)
      self.top_item = item
      self.size += 1
    else:
      print("All out of space!")
 
  def pop(self):
    if self.size > 0:
      item_to_remove = self.top_item
      self.top_item = item_to_remove.get_next_node()
      self.size -= 1
      return item_to_remove.get_value()
    else:
      print("This stack is totally empty.")
 
  def peek(self):
    if self.size > 0:
      return self.top_item.get_value()
    else:
      print("Nothing to see here!")
 
 
  def is_empty(self):
    return self.size == 0
```







## Використані джерела
Практикум програмування Python/C++ на eolymp.com
Пришвидшений курс Python
https://www.codecademy.com/learn/python-for-programmers

TechWorld with Nana. Python Tutorial for Beginners (https://youtu.be/t8pPdKYpowI)












